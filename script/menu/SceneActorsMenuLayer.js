import {MODULE_ID} from "../SceneActors.js";

export default class SceneActorsMenuLayer extends InteractionLayer {

    constructor() {
        super();

        Hooks.on('getSceneControlButtons', (btns) => {

            const tokenControls = btns.find(c => c.name === "token");

            if (game.user.isGM) {
                tokenControls.tools.push({
                    name: "toggleSceneActors",
                    title: game.i18n.localize("Menu.enableSceneActorsOnScene"),
                    icon: "far fa-street-view",
                    toggle: true,
                    active: !!game.scenes.active?.getFlag(MODULE_ID, 'show'),
                    onClick: () => game.yendorsSceneActors.toggleSceneActors()
                });
            } else {
                tokenControls.tools.push({
                    name: "toggleSceneActorsTemp",
                    title: game.i18n.localize("Menu.hideSceneActorsOnScene"),
                    icon: "far fa-street-view",
                    visible: !!game.scenes.active?.getFlag(MODULE_ID, 'show'),
                    active: !game.yendorsSceneActors.hideActorsTemp ?? false,
                    toggle: true,
                    onClick: () => game.yendorsSceneActors.toggleActorsTemp()
                });
            }
        });

    }

    selectObjects(optns) {
        canvas.tokens.selectObjects(optns)
    }
}